# Utilizando listas seguras con hilos sin bloqueo

Una de las estructuras de datos más utilizada es la **lista**. Una lista tiene un número indeterminado de elementos y se podrá añadir, consultar o eliminar un elemento en cualquier posición. Las listas, en aplicaciones concurrentes, permiten que varios hilos añadir o eliminar elementos al mismo tiempo sin que se produzcan inconsistencia en los datos.

En el ejemplo, se enseñará la forma para utilizar listas sin bloqueo en los programas. Las listas sin bloqueo proporcionan operaciones que, si la operación no puede completarse inmediatamente (por ejemplo, queremos obtener un elemento en una lista vacía) se *lanzará una excepción* o devolverá el valor `null`, dependiendo de la operación. **Java 7** incluye la clase [`ConcurrentLinkedDeque`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ConcurrentLinkedDeque.html) que implementa las listas concurrentes sin bloqueo.

Implementaremos un ejemplo con las siguientes dos tareas:

- Una tarea que añadirá datos a la lista de forma masiva.
- Una tarea que eliminará datos de la misma lista de forma masiva.

1. Definimos una clase llamada `AddTask` que implementa la interface `Runnable`. Es la clase que implementa la tarea para añadir elementos a la lista.
2. Necesitamos una variable de instancia para la lista donde se añadirán los elementos de tipo `String`. La clase de la lista será `ConcurrentLinkedDeque` que estará parametrizada por el tipo de elemento que la forma.
3. El constructor de la clase dará valor a la variable de instancia.
4. Implementamos el método `run()` para añadir los elementos a la lista. Para ello obtenemos el nombre del hilo que ejecuta la tarea y añadiremos `10000` elementos donde estará el nombre del hilo y el número de ejecución del bucle.
5. Para añadir utilizamos el método `add(.)` de la lista, que añade un elemento al final de la lista. 

```java
...
@Override
public void run() {
    String name=Thread.currentThread().getName();
    for (int i=0; i<10000; i++){
        list.add(name+": Element "+i);
    }
}
...
```

6. Definimos una nueva clase llamada `PoolTask` que implementa la interface `Runnable` que implementará la tarea que elimina los elementos de la lista.
7. Como en la clase anterior, necesitamos una variable de instancia para almacenar la lista de la clase `ConcurrentLinkedDeque`.
8. El constructor de la clase será el que establezca el valor para la variable de instancia.
9. Implementamos el método `run()` para eliminar los elementos de la lista. Para ello realizamos `5000` operaciones que eliminarán el primer elemento de la lista, mediante el método `poolFirst()`, y el último elemento de la lista, mediante el método `poolLast()`.

```java
...
@Override
public void run() {
    for (int i=0; i<5000; i++) {
        list.pollFirst();
        list.pollLast();
    }
}
...
```

10. Ahora implementamos el método `main(.)` de la aplicación que nos permitirá probar las tareas implementadas anteriormente.
11. Creamos una lista de la clase `ConcurrentLinkedDeque` que almacena elementos de la clase `String`.
12. Creamos un array para `100` objetos de la clase `Thread` que nos permitirá realizar las tareas de inserción y borrado en la lista.
13. Como primer paso, creamos tantas tareas `AddTask` como hilos. Asociamos las tareas a los hilos y ejecutamos los hilos.
14. Esperamos hasta que se completen las tareas de inserción y mostramos por la salida estándar el número de elementos que se han añadido a la lista.
15. Ahora creamos tantas tareas `PoolTask` como hilos disponibles. Asociamos las tareas a los hilos y ejecutamos los hilos.
16. Esperamos a que finalicen las tareas de borrado y mostramos el estado de la lista por la salida estándar. El resultado es que la se han realizados tantas tareas de inserción como de borrado y por tanto la lista deberá terminar vacía. Comprobad que este es el resultado.

## Información Adicional

La clase [`ConcurrentLinkedDeque`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ConcurrentLinkedDeque.html) proporciona más métodos que nos permiten obtener elementos de la lista:

- `getFirst()` y `getLast()`: Estos métodos nos devuelven el primer y último elemento de la lista respectivamente. Los métodos no eliminan el elemento de la lista. Si la lista está vacía, estos métodos lanzan la excepción `NoSuchElementException`.
- `peek()`, `peekFirst()` y `peekLast()`: Estos métodos devuelven el primer y último elemento de la lista respectivamente. Los métodos no eliminan el elemento de la lista. Si la lista está vacía, estos métodos devuelven el valor `null`.
- `remove()`, `removeFirst()` y `removeLast()`: Estos métodos devuelven el primer y último elemento de la lista respectivamente. Los métodos eliminan el elemento de la lista. Si la lista está vacía, estos métodos lanzan la excepción `NoSuchElementException`.

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE1Mjc5NzMxNzhdfQ==
-->