[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# GUIÓN DE PRÁCTICAS[^nota1]
## Sesión 8: Biblioteca de Estructuras Concurrentes (I)

Las **Estructuras de Datos** son un elemento básico en la programación. Los programas utilizan uno o más tipos de estructuras de datos para almacenar y gestionar sus datos. El [API de Java](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/package-frame.html) nos proporciona una biblioteca de estructuras de datos donde encontramos interfaces, clases y algoritmos implementados para que podamos utilizarlas en nuestros programas.

Cuando utilizamos una estructura de datos en un programa concurrente, debemos conocer cómo se ha realizado su implementación. Tenemos clases que no están preparadas para trabajar dentro de aplicaciones concurrentes porque no controlan el acceso que se puede hacer desde diferentes hilos. Como ya hemos presentado en las clases de teoría, cuando diferentes tareas quieren acceder de forma concurrente a una misma estructura de datos, sin garantizar la **exclusión mutua**, podemos encontrarnos con problemas de consistencia en los datos que afectará al correcto funcionamiento de nuestra aplicación. Un ejemplo de este tipo de estructuras de datos es la clase [`ArrayList`](https://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html).

Java nos proporciona una biblioteca de **estructuras seguras** para que podamos utilizarlas en nuestros programas sin problemas de inconsistencia. En Java tenemos dos tipos de estructuras que podemos utilizar:

- **Estructuras con bloqueo**: Este tipo de estructuras incluyen operaciones para añadir y eliminar datos. Si la operación no puede completarse inmediatamente, porque la estructura está llena o vacía, el hilo que realiza la llamada se bloquea hasta que pueda completar la operación.
 
- **Estructura sin bloqueo**: Este tipo de estructuras incluyen operaciones para añadir y eliminar natos. Si la operación no puede completarse inmediatamente, la operación devolverá `null` o *lanzará una excepción*, pero el hilo que realiza la llamada no se bloqueará.

En los ejemplos que se presentarán en el guión, se aprenderá la forma para utilizar algunas de las estructuras de datos en Java para nuestros programas. Estas son:

- Listas sin bloqueo, utilizando la clase [`ConcurrentLinkedDeque`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ConcurrentLinkedDeque.html).

- Listas con bloqueo, utilizando la clase [`LinkedBlockingDeque`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/LinkedBlockingDeque.html).

- Listas con bloqueo que ordenan sus elementos por prioridad, utilizando la case [`PriorityBlockingQueue`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/PriorityBlockingQueue.html).

- Listas con bloqueo con elementos retardados, utilizando la clase [`DelayQueue`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/DelayQueue.html).

Los ejemplos que muestran la utilización de estas clases son:

1. [Utilizando listas seguras con hilos sin bloqueo](https://gitlab.com/ssccdd/guionsesion8/-/blob/master/Ejercicio1.md)
2. [Utilizando lista seguras con hilos con bloqueo](https://gitlab.com/ssccdd/guionsesion8/-/blob/master/Ejercicio2.md)
3. [Utilizando listas seguras ordenadas por prioridad con hilos con bloqueo](https://gitlab.com/ssccdd/guionsesion8/-/blob/master/Ejercicio3.md)
4. [Utilizando listas seguras con elementos retardados](https://gitlab.com/ssccdd/guionsesion8/-/blob/master/Ejercicio4.md)

---
[^nota1]: El guión se extrae del libro *Java 7 Concurrency Cookbook* que se encuentra disponible para los alumnos de la [Universidad de Jaén](https://www.ujaen.es/) por medio de su servicio de [Biblioteca Digital](http://www.ujaen.debiblio.com/login?url=https://learning.oreilly.com/home/).
<!--stackedit_data:
eyJoaXN0b3J5IjpbNzEwNTM1ODIwLDI4NTc4NzY5MV19
-->