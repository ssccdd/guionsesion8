# Utilizando listas seguras ordenadas por prioridad con hilos con bloqueo

Cuando se trabajan con estructuras de datos del tipo **lista**, una de las características que pueden tener, es que los datos estén ordenados. Java nos proporciona la clase [`PriorityBlockingQueue`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/PriorityBlockingQueue.html) que tiene esta funcionalidad.

Todos los elementos que se quieran añadir a un objeto de la clase `PriorityBlockingQueue` tienen que implementar la interface [`Comparable`](https://docs.oracle.com/javase/8/docs/api/java/lang/Comparable.html). Esta interface tiene el método `compareTo(.)` que recibe un objeto del mismo tipo, por tanto tenemos dos objetos a comparar: uno es el que está invocando el método y el otro el que se pasa como parámetro. El método debe devolver un número menor que cero si el objeto que invoca el método es menor que el del parámetro, un número mayor que cero si el objeto que invoca el método es mayor que el del parámetro, y cero si ambos objetos son iguales.

La clase `PriorityBlockingQueue` usa el método `compareTo(.)` cuando se inserta un elemento y determina la posición donde se insertará. La ordenación de la lista será de mayor a menor. Otra característica importante de la clase `PriorityBlockingQueue` es que es una **estructura con bloqueo**. Tiene métodos que, si no pueden completar la operación en el momento, se bloquea el hilo hasta que pueda completarla.

En este ejemplo, se utilizará la clase `PriorityBlockingQueue` para almacenar una serie de eventos con diferentes prioridades en la misma lista.

1. Definimos una clase llamada `Event` que implementa la interface `Comparable` que estará parametrizada con el mismo tipo de la clase `Event`.
2. Creamos dos variables de instancia que nos permitirán almacenar:
	- Un entero que nos indicará el hilo que creó el evento.
	- Un entero que nos indicará la prioridad del evento.
3. Creamos un constructor para dar valor a las variables de instancia.
4. Creamos dos métodos de instancia para consultar el valor de las variables de instancia. Revisar el código de ejemplo del guión.
5. Solo queda implementar el método `comparable(.)` que nos permitirá ordenar dos eventos. La ordenación se realizará comparando las prioridades de cada uno de los eventos a comparar.

```java
...
/**
 * Method that compares two events and decide which has more priority
 */
@Override
public int compareTo(Event e) {
    if (this.priority>e.getPriority()) {
        return -1;
    } else if (this.priority<e.getPriority()) {
        return 1; 
    } else {
        return 0;
    }
}
...
```

6. Definimos una clase llamada `Task` que implementa la interface `Runnable`.
7. La clase tiene dos variables de instancia:
	- Un entero para almacenar el identificador de la tarea.
	- Un objeto de la clase `PriorityBlockingQueue` que almacenará los eventos ordenados por prioridad.
8. Las variables de instancia obtienen su valor mediante el constructuctor creado para la clase. Revisar el código de ejemplo del guión.
9. Implementamos el método `run()`. El método simula la creación de `1000` eventos asociados a la tarea y con prioridad creciente y que se añadirán a la lista. El método utilizado para ello es `add(.)`.

```java
...
/**
 * Main method of the task. It generates 1000 events and store
 * them in the queue
 */
@Override
public void run() {
    for (int i=0; i<1000; i++){
        Event event=new Event(id,i);
        queue.add(event);
    }
}
...
```

10. Ahora implementamos el método `main(.)` para realizar la prueba del uso de una lista `PriorityBlockingQueue`.
11. Creamos el objeto que representa la lista.
12. Creamos un array para `5` objetos de la clase `Thread`.
13. Creamos tareas de la clase `Task` según los hilos disponibles a las que se le asignará un identificador y todas ellas compartirán la misma lista. Se asociarán las tareas a los hilos disponibles para su ejecución.
14. Se esperará hasta la finalización de las tareas.
15. Para finalizar se presentarán los elementos almacenados en la lista y se puede comprobar que se han insertado ordenados por la prioridad. Revisar el código de ejemplo del guión.

## Información Adicional

La clase [`PriorityBlockingQueue`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/PriorityBlockingQueue.html) proporciona otros métodos que tienen características interesantes:

- `clear()`: Este método elimina todos los elementos de la lista.
- `take()`: Este método devuelve y elimina el primer elemento de la lista. Si la lista está vacía, el método bloquea el hilo que lo ha invocado hasta que haya elementos.
- `put( E e)`: `E` es la clase de elementos que se almacenan en la lista `PriorityBlockingQueue`. El método inserta el elemento pasado como parámetro en la lista.
- `peek()`: Este método devuelve el primer elemento de la lista, pero no lo elimina.
 
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTQ5NzkwMzEwXX0=
-->