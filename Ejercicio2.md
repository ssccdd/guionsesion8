# Utilizando lista seguras con hilos con bloqueo

En este ejemplo, se mostrará la forma de trabajar con listas **con bloqueo**. La mayor diferencia entre las listas **con bloqueo** y **sin bloqueo** está en los métodos para añadir o eliminar elementos en ellas, si la operación no se puede completar en el momento, el hilo que realizó la llamada se bloquea hasta que se complete la operación. Java incluye la clase [`LinkedBlockingDeque`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/LinkedBlockingDeque.html) que implementa las listas con bloqueo.

1. Definimos una clase llamada `Client` que implementa la interface `Runnable`.
2. Tendrá una variable de instancia que será una lista de la clase `LinkedBlockingDeque` que almacenará elementos de la clase `String`. La variable de instancia obtendrá su valor mediante el constructor de la clase.
3. Implementamos el método `run()` para realizar las acciones de la tarea. Las acciones, consisten en añadir `5` elementos por `3` veces en la lista mediante el método `put(.)`.

```java
...
@Override
public void run() {
    for (int i=0; i<3; i++) {
        for (int j=0; j<5; j++) {
            StringBuilder request=new StringBuilder();
            request.append(i);
            request.append(":");
            request.append(j);
            try {
                requestList.put(request.toString());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.printf("Client: %s at %s.\n",request,new Date());
        }
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
        
    System.out.printf("Client: End.\n");
}
...
```

4. Ahora implementamos el método `main(.)` para realizar la prueba de la tarea `Client`.
5. Creamos la lista de la clase `LinkedBlockingDeque` que almacena elementos de la clase `String`.
6. Creamos una tarea `Client` y un hilo para poder ejecutarla.
7. Posteriormente, obtendremos `3` elementos de la lista, repitiendo la operación `5` veces, y la presentaremos en la salida estándar. Para ello utilizamos el método `take()`. Entre operación y operación, incluimos un retardo de `300` milisegundos.

```java
...
for (int i=0; i<5 ; i++) {
    for (int j=0; j<3; j++) {
        String request=list.take();
        System.out.printf("Main: Request: %s at %s. Size: %d\n",request,new Date(),list.size());
    }
    TimeUnit.MILLISECONDS.sleep(300);
}
...
```

## Información Adicional

La clase [`LinkedBlockingDeque`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/LinkedBlockingDeque.html) proporciona otros métodos para añadir y eliminar elementos de la lista que, en vez de bloquearse, *lanzan una excepción* o devuelven el valor `null`. Estos métodos son:

- `takeFirst()` y `takeLast()`: Estos métodos devuelven el primer y último elemento de la lista respectivamente. Se elimina el elemento de la lista. Si la lista está vacía, estos métodos bloquean el hilo hasta haya elementos en la lista.
- `getFirst()` y `getLast()`: Estos métodos devuelven el primer y último elemento de la lista respectivamente. No se elimina el elemento de la lista. Si la lista está vacía, estos métodos lanzan la excepción `NoSuchElementException`.
- `peek()`, `peekFirst()` y `peekLast()`: Estos métodos devuelven el primer y el último elemento de la lista respectivamente. No se elimina el elemento de la lista. Si la lista está vacía, estos métodos devuelven el valor `null`.
- `poll()`, `pollFirst()` y `pollLast()`: Estos métodos devuelven el primer y el último elemento de la lista respectivamente. Se elimina el elemento de la lista. Si la lista está vacía, estos métodos devuelven el valor `null`.
- `add()`, `addFirst()` y `addLast()`: Estos métodos añaden en el primera y última posición de la lista respectivamente. Si la lista está llena (se ha creado la lista con una capacidad máxima), estos métodos lanzan la excepción `IllegalStateException`.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE4Mzg3NzY2NDNdfQ==
-->