# Utilizando listas seguras con elementos retardados

Una estructura útil que nos proporciona el **API de Java**, y que se puede utilizar en las aplicaciones concurrentes, es la implementada por la clase [`DelayedQueue`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/DelayQueue.html). En esta clase, se pueden almacenar elementos con una activación programada por fecha. Los métodos que devuelven o eliminan elementos de la lista ignorarán los elementos cuya fecha de activación aún no se ha producido. Esto es, no son visibles para estos métodos.

Para obtener este comportamiento, los elementos que se deseen almacenar en un objeto de la clase `DelayedQueue` tienen que implementar la interface [`Delayed`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Delayed.html). Esta interface permite trabajar con elementos programados por fecha, esto es, se puede establecer la fecha de activación de los objetos almacenados en un objeto de la clase `DelayedQueue` como el tiempo que falta hasta la fecha de activación. La interface `Delayed` obliga a que se tengan que implementar los métodos:

- `compareTo(Delayed o)`: La interface `Delayed` hereda de la interface `Compare`. Este método devolverá un valor menor a cero si el objeto que invoca el método tiene un tiempo programado menor que el objeto pasado como parámetro, un valor mayor a cero si el objeto que invoca el método tiene un tiempo programado mayor que el objeto pasado como parámetro, y cero si ambos objetos tienen el mismo tiempo programado.
- `getDelay(TimeUnit unit)`: Este método devuelve el tiempo que resta hasta la fecha de activación en la unidad que se especifica como parámetro. La clase [`TimeUnit`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/TimeUnit.html) es una clase con una enumeración de las siguientes constantes: `DAYS`, `HOURS`, `MICROSECONDS`, `MILLISECONDS`, `MINUTES`, `NANOSECONDS` y `SECONDS`.

En el ejemplo, se aprenderá a utilizar la clase `DelayedQueue` para almacenar una serie de eventos con diferentes fechas de activación.

1. Definimos la clase `Event` que implementa la interface `Delayed`. Esta clase representa los elementos que serán almacenados en la lista.
2. Como variable de instancia, necesitamos un objeto de la clase [`Date`](https://docs.oracle.com/javase/8/docs/api/java/util/Date.html) que nos permitirá establecer la fecha programada para el evento.
3. El constructor de la clase establecerá el valor de la variable de instancia. Revisar el código de ejemplo del guión.
4. Debemos implementar el método `compareTo(.)` para saber si la fecha programada del objeto que invoca el método es menor, mayor o igual que el objeto que se pasa como parámetro.

```java
...
/**
 * Method to compare two events
 */
@Override
public int compareTo(Delayed o) {
    long result=this.getDelay(TimeUnit.NANOSECONDS)-o.getDelay(TimeUnit.NANOSECONDS);
    if (result<0) {
        return -1;
    } else if (result>0) {
        return 1;
    }
    return 0;
}
...
```

5. El otro método que debemos implementar es `getDelay(.)` para que nos devuelva el tiempo que falta para alcanzar la fecha programada en el unidad de tiempo especificada como parámetro.

```java
...
/**
 * Method that returns the remaining time to the activation of the event
 */
@Override
public long getDelay(TimeUnit unit) {	
    Date now=new Date();
    long diff=startDate.getTime()-now.getTime();
    return unit.convert(diff,TimeUnit.MILLISECONDS);
}
...
```

6. Definimos una clase `Task` que implementa la interface `Runnable`. Esta clase representa la tarea que realizaremos en nuestro programa.
7. Como variables de instancia tendremos:
	- Un entero que almacenará el identificador de la tarea.
	- Un objeto de la clase `DelayQueue` donde se almacenarán los objetos `Event`.
8. El constructor de la clase establecerá los valores de las variables de instancia.
9. Implementamos el método `run()` para implementar lo que debe realizar la tarea.
10. Obtenemos dos objetos de la clase `Date` para programar el retardo de la ejecución. Mostramos ese tiempo en la salida estándar.
11. Añadimos `100` elementos `Event` con el mismo tiempo de activación a la lista.

```java
...
@Override
public void run() {
    Date now=new Date();
    Date delay=new Date();
    delay.setTime(now.getTime()+(id*1000));

    System.out.printf("Thread %s: %s\n",id,delay);
		
    for (int i=0; i<100; i++) {
        Event event=new Event(delay);
        queue.add(event);
    }
}
...
```

12. Implementamos el método `main(.)` de la aplicación principal para probar las características de la lista al añadir y eliminar elementos.
13. Creamos un objeto de la clase `DelayedQueue` que almacene elementos `Event`.
14. Creamos un array para `5` objetos `Thread`.
15. Creamos objetos de la clase `Task` igual al número de hilos disponibles. Asociamos las tareas a los hilos para su ejecución.
16. Esperamos a que finalicen las tareas.
17. Presentamos por la salida estándar los elementos que están en la lista mediante el método `poll()`. Podemos ver que sólo se pueden extraer elementos de la lista cuando llegan a su activación. Si no hay elementos activos, no se extraen, pero la lista sí nos dice que hay elementos disponibles.

```java
...
/*
 * Write the results to the console
 */
do {
    int counter=0;
    Event event;
    do {
        event=queue.poll();
        if (event!=null) counter++;
    } while (event!=null);
    System.out.printf("At %s you have read %d events\n",new Date(),counter);
    TimeUnit.MILLISECONDS.sleep(500);
} while (queue.size()>0);
...
```

## Información Adicional

La clase [`DelayQueue`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/DelayQueue.html) tiene otros métodos que se presentan a continuación:

- `clear()`: Este método elimina todos los elementos de la lista.
- `offer(E e)`: `E` representa la clase de los elementos que almacena la lista. Este método inserta el elemento pasado como parámetro en la lista.
- `peek()`: Este método obtiene, pero no elimina, el primer elemento de la lista.
- `take()`: Este método obtiene y elimina el primer elemento de la lista. Si no hay elementos activos en la lista, el hilo que invoca el método se bloquea hasta que haya algún elemento activo.


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE4NDQyNDgyNjRdfQ==
-->